package fr.cnam.foad.nfa035.badges.gui;

import fr.cnam.foad.nfa035.badges.gui.BadgeWalletGUI;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.jdesktop.swingx.JXDatePicker;

import javax.swing.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class AddBadgeDialog extends JDialog implements PropertyChangeListener {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField codeSerie;
    private JFileChooser fileChooser;
    private JXDatePicker dateDebut;
    private JXDatePicker dateFin;

    private DirectAccessBadgeWalletDAO dao;



    private DigitalBadge digitalBadge;

    private BadgeWalletGUI badgeWalletGUI;

    public void setDao(DirectAccessBadgeWalletDAO dao) {
        this.dao = dao;
    }

    public void setCaller(BadgeWalletGUI badgeWalletGUI) {
        this.badgeWalletGUI = badgeWalletGUI;
    }

    /**
     * method faisant apparitre le AddBadge dialogue
     */
    public AddBadgeDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    /**
     * permet de récupérer un icon
     * @return ImageIcon
     */
    public ImageIcon getIcon(){
        return new ImageIcon(getClass().getResource("/logo.png"));
    }


    /**
     * action lorsque l'on clique sur le bouton ok
     */
    private void onOK() {
        // add your code here
        dispose();
    }

    /**
     * action lorsque l'on clique sur le bouton cancel
     */
    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

    public static void main(String[] args) {
        AddBadgeDialog dialog = new AddBadgeDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }

    /**
     * This method gets called when a bound property is changed.
     *
     * @param evt A PropertyChangeEvent object describing the event source
     *            and the property that has changed.
     */
    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (validateForm()){
            this.buttonOK.setEnabled(true);
        }
        else{
            this.buttonOK.setEnabled(false);
        }

    }

    /**
     * permet de vérifier si le form est valide
     * @return un boolean
     */
    private boolean validateForm() {
        return false;
    }
}
